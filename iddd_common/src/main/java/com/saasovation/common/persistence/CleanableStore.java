package com.saasovation.common.persistence;

public interface CleanableStore {

    public void clean();
}
