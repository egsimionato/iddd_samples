package com.saasovation.common.domain.model;

public interface Identity {

    public String id();
}
