package com.saasovation.common.notification;

public interface NotificationPublisher {

    public void publishNotifications();

    public boolean internalOnlyTestConfirmation();
}
