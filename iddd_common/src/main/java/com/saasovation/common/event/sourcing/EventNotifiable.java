package com.saasovation.common.event.sourcing;

public interface EventNotifiable {

    public void notifyDispatchableEvents();
}
